stages:
    - build

default:
    before_script:
        - set -x
        - export VLC_CONTRIB_SHA="$(extras/ci/get-contrib-sha.sh)"
        - export VLC_PREBUILT_CONTRIBS_URL="https://artifacts.videolan.org/vlc-3.0/${CI_JOB_NAME##nightly-}/vlc-contrib-${TRIPLET}-${VLC_CONTRIB_SHA}.tar.bz2"
        - if ! extras/ci/check-url.sh "$VLC_PREBUILT_CONTRIBS_URL"; then unset VLC_PREBUILT_CONTRIBS_URL; fi
    after_script:
        - export VLC_CONTRIB_SHA="$(extras/ci/get-contrib-sha.sh)"
        - mv contrib/vlc-contrib-*.tar.bz2 contrib/vlc-contrib-${TRIPLET}-${VLC_CONTRIB_SHA}.tar.bz2 2>/dev/null || true
    interruptible: true

variables:
    VLC_WIN32_IMAGE: registry.videolan.org/vlc-debian-win32-3.0:20211008142723
    VLC_WIN64_IMAGE: registry.videolan.org/vlc-debian-win64-3.0:20211008140026
    VLC_ANDROID_IMAGE: registry.videolan.org/vlc-debian-android-3.0:20211004103136
    
.variables-win32: &variables-win32
        SHORTARCH: win32
        HOST_ARCH: i686
        TRIPLET: $HOST_ARCH-w64-mingw32

.variables-win64: &variables-win64
        SHORTARCH: win64
        HOST_ARCH: x86_64
        TRIPLET: $HOST_ARCH-w64-mingw32

.variables-android-arm: &variables-android-arm
        ANDROID_ARCH: arm
        TRIPLET: arm-linux-androideabi

.variables-android-arm64: &variables-android-arm64
        ANDROID_ARCH: arm64
        TRIPLET: aarch64-linux-android

.variables-android-x86: &variables-android-x86
        ANDROID_ARCH: x86
        TRIPLET: i686-linux-android

# Common rules
.base-template:
    stage: build
    #only:
        #refs:
            #- merge_requests
            #- 3.0.x@videolan/vlc
    except:
        - schedules
    artifacts:
        paths:
            - contrib/vlc-contrib-${TRIPLET}-*.tar.bz2

# Common rules for jobs using docker
.docker-template:
    extends: .base-template
    tags:
        - docker
        - amd64

#
# Windows
#
.win-common:
    extends: .docker-template
    script: |
        if [ "${CI_JOB_NAME:0:8}" = "nightly-" ]; then
            NIGHTLY_EXTRA_BUILD_FLAGS="-i n -l"
        fi
        if [ -n "$VLC_PREBUILT_CONTRIBS_URL" ]; then
            echo "Building using prebuilt contribs at $VLC_PREBUILT_CONTRIBS_URL"
            extras/package/win32/build.sh -p -a $HOST_ARCH $NIGHTLY_EXTRA_BUILD_FLAGS $LIBVLC_EXTRA_BUILD_FLAGS $UWP_EXTRA_BUILD_FLAGS
        else
            extras/package/win32/build.sh -c -a $HOST_ARCH $NIGHTLY_EXTRA_BUILD_FLAGS $LIBVLC_EXTRA_BUILD_FLAGS $UWP_EXTRA_BUILD_FLAGS
        fi

win32:
    extends: .win-common
    image:
        name: $VLC_WIN32_IMAGE
    variables: *variables-win32

win64:
    extends: .win-common
    image:
        name: $VLC_WIN64_IMAGE
    variables: *variables-win64

.release-win-common:
    # We don't use any of the .win-common template so just use .docker-template
    extends: .docker-template
    only:
      - tags
    tags:
        - release
        - amd64
    before_script:
      # We usually can use $CI_COMMIT_TAG, but this lets us do fixup tags
      # such as 3.0.123-1
      # This is done in a before_script because we want to bypass the
      # prebuilt contrib logic but can't have an empty before_script
      - export RELEASE_TAG=$(echo $CI_COMMIT_TAG | cut -f 1 -d -)
    script:
      - export PATH=/opt/breakpad/bin:$PATH
      - ./extras/package/win32/build.sh -r -l -i u -a $HOST_ARCH -b https://win.crashes.videolan.org
    artifacts:
      paths:
        - $CI_PROJECT_DIR/$SHORTARCH/vlc-*release.7z

release-win32:
    extends: .release-win-common
    image:
        name: $VLC_WIN32_IMAGE
    variables: *variables-win32

release-win64:
    extends: .release-win-common
    image:
        name: $VLC_WIN64_IMAGE
    variables: *variables-win64

#
# Android
#
.android-common:
    extends: .docker-template
    image:
        name: $VLC_ANDROID_IMAGE
    script: |
        wget https://code.videolan.org/videolan/vlc-android/raw/0daaf5f3a08b5c52b4caaf526633cca7061d04c2/compile-libvlc.sh
        if [ -n "$VLC_PREBUILT_CONTRIBS_URL" ]; then
            /bin/sh ./compile-libvlc.sh -a $ANDROID_ARCH --with-prebuilt-contribs
        else
            /bin/sh ./compile-libvlc.sh -a $ANDROID_ARCH --package-contribs
        fi
    artifacts:
        paths:
            - build-android-${TRIPLET}

android-arm:
    extends: .android-common
    variables: *variables-android-arm

android-arm64:
    extends: .android-common
    variables: *variables-android-arm64

android-x86:
    extends: .android-common
    variables: *variables-android-x86